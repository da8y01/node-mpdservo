const http    = require('http')
const express = require('express')
const api     = require('./api')

const app     = express()
const server  = http.Server(app)
const PORT    = process.env.PORT || 8080
const stat    = express.static(__dirname + '/static')
const statIdx = express.static(__dirname + '/static/index.html')

app.use('/api', api.app)
// app.use('/old/', express.static(__dirname + '/static/old'))
app.use('/', statIdx)
app.use('/options', statIdx)
app.use('/songs', statIdx)
app.use('/videos/:id', statIdx)
app.use('/songs/:id', statIdx)
app.use('/', stat)

server.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log('listening on *:' + PORT)
})
