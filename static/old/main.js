;
(function () {
  'use strict';
  const _i = document.getElementById.bind(document)
  const _q = document.querySelector.bind(document)
  const _a = document.querySelectorAll.bind(document)
  const _c = document.createElement.bind(document)
  const _t = document.createTextNode.bind(document)
  const _l = console.log.bind(console)
  const _e = console.error.bind(console)

  const submitAll = _i('submit_all')
  const container = _i('videos')

  _i('ytlist').addEventListener('submit', (ev) => {
    ev.preventDefault()
    const btn = ev.target.querySelector('button[type=submit]')
    show(submitAll, false)
    let inp = ev.target.querySelector('#ytlist_id')
    let id = getParam(inp.value)
    if (id == '') {
      return
    }
    inp.value = id
    let url = ev.target.action + id
    btn.classList.add('is-loading')
    fetch(url).then(resp => resp.json()
    ).then(json => {
      showVideos(json)
      show(submitAll, true)
      btn.classList.remove('is-loading')
    })
  })
  submitAll.addEventListener('click', submitVideos)

  _i('clear_list').addEventListener('click', ev => {
    ev.preventDefault()
    ev.target.classList.add('is-loading')
    fetch('/clear').then(resp => {
      ev.target.classList.remove('is-loading')
    })
  })

  _i('pause').addEventListener('click', ev => {
    ev.preventDefault()
    ev.target.classList.add('is-loading')
    fetch('/pause').then(resp => {
      ev.target.classList.remove('is-loading')
    })
  })

  const modal = _i('playlist_modal')

  document.addEventListener('keydown', ev => {
    if (ev.keyCode == 27) {// 27 -> Esc
      modal.classList.remove('is-active')
    }
  })

  modal.addEventListener('click', ev => {
    if (ev.target.classList.contains('modal-background')
      || ev.target.classList.contains('modal-close')) {
      ev.preventDefault()
      modal.classList.remove('is-active')
    }
  })
  _i('playlist').addEventListener('click', ev => {
    ev.preventDefault()
    ev.target.classList.add('is-loading')
    fetch('/playlist').then(resp => resp.json()).then(json => {
      ev.target.classList.remove('is-loading')
      const list = modal.querySelector('.box ul')
      emptyEl(list)
      json.songs.forEach((s, i) => {
        const li = _c('li')
        s = s.replace(/(_|;;;)/g, ' ')
        let el = _t(s)
        if (i == json.status.song) {
          const strong = _c('strong')
          strong.appendChild(el)
          el = strong
        }
        li.appendChild(el)
        list.appendChild(li)
      })
      modal.classList.add('is-active')
    })
  })

  container.addEventListener('click', ev => {
    if (ev.target.tagName.toUpperCase() == 'A' && ev.target.classList.contains('related')) {
      ev.preventDefault()
      const id = ev.target.hash.substr(1)
      _i('ytlist_id').value = id
      _q('#ytlist button[type=submit]').click()
    }
  })

  function showVideos(videos) {
    emptyEl(container)
    videos.video.forEach(vid => container.appendChild(buildVideo(vid)))
  }

  function buildVideo(vid) {
    const id = vid.encrypted_id
    const thumb = vid.thumbnail
    const duration = vid.duration
    const title = vid.title + ' ' + duration
    const href = 'https://youtu.be/' + id

    const card = h('div', {class: 'card'})
    const cont = h('div', {class: 'column is-one-quarter-desktop is-half-tablet'})
    cont.appendChild(card)

    card
      .appendChild(h('div', {class: 'card-image'}))
        .appendChild(h('figure', {class: 'image is-4by3'}))
          .appendChild(h('a', {title, href, target: 'youtube'}))
            .appendChild(h('img', {alt: title, src: thumb}))
    card
      .appendChild(h('div', {class: 'card-content'}))
        .appendChild(h('div', {class: 'media-content'}))
          .appendChild(h('div', {class: 'title is-6 has-text-centered'}))
            .appendChild(h('a', {title, href, target: 'youtube'}))
              .appendChild(_t(title))

    const cardContent = card.appendChild(h('div', {class: 'content'}))
    const checkAndLabel = h('div', {class: 'title is-6 has-text-centered'})
    cardContent.appendChild(checkAndLabel)
    checkAndLabel
      .appendChild(h('input', {id: 'check_' + id, type: 'checkbox', checked: true, 'data-id': id, 'data-title': title}))
    checkAndLabel
      .appendChild(h('label', {for: 'check_' + id}))
        .appendChild(_t('select this video'))
    checkAndLabel.appendChild(h('br'))
    checkAndLabel
      .appendChild(h('a', {title: 'Related', href: '#RD' + id, class: 'related'}))
        .appendChild(_t('show related videos'))

    return cont
  }

  function submitVideos(ev) {
    ev.preventDefault()
    const checks = container.querySelectorAll('input[type=checkbox]:checked')
    let vids = []
    for (let i = 0; i < checks.length; i++) {
      vids.push({
        id: checks[i].dataset.id,
        title: checks[i].dataset.title
      })
    }
    return fetch('/list', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify(vids)
    })
  }

  function relate(ev) {
    ev.preventDefault()
    const id = ev.target.href.substr(1)
    _i('ytlist_id').value = id
    _q('#ytlist button[type=submit]').click()
  }

  function getParam(url) {
    url = url.trim().replace(/.*\//, '')
    url = url.replace(/.*watch\?/, '')
    const params = url.split('&').map(el => el.split('='))
    if (params.length == 1 && params[0].length == 1) {
      let out = params[0][0]
      if (out.length && out.length < 12) {
        out = 'RD' + out
      }
      return out
    }
    let out = ''
    for (let i = 0; i < params.length; i++) {
      if (params[i][0] == 'v' && out == '') {
        out = params[i][1]
      }
      if (params[i][0] == 'l' || params[i][0] == 'list') {
        out = params[i][1]
      }
    }
    if (out.length && out.length < 12) {
      out = 'RD' + out
    }
    return out
  }

  function emptyEl(element) {
    const length = element.children.length
    for (let i = 0; i < length; i++) {
      element.removeChild(element.children[0])
    }
  }

  function show(el, bul) {
    if (bul) {
      el.classList.remove('is-invisible')
      return el
    }
    el.classList.add('is-invisible')
    return el
  }

  function h(tagName, attributes = {}) {
    let tag = document.createElement(tagName)
    for (let attr in attributes) {
      tag.setAttribute(attr, attributes[attr])
    }
    return tag
  }
})()
