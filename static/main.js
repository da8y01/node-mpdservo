(function () {
  'use strict';

  var i = document.getElementById.bind(document);
  var q = document.querySelector.bind(document);
  var a = document.querySelectorAll.bind(document);
  var c = document.createElement.bind(document);
  var t = document.createTextNode.bind(document);
  // eslint-disable-next-line no-console
  var l = console.log.bind(console);
  // eslint-disable-next-line no-console
  var e = console.error.bind(console);

  var base = window.localStorage.getItem('yt2mpd_base') || '/api';
  window.localStorage.setItem('yt2mpd_base', base);

  var u = {
    i: i, q: q, a: a, c: c, t: t, l: l, e: e,
    setActive: setActive,
    base: base
  };

  function setActive(url) {
    a('#navMenu a[data-type=route]').forEach(function (anchor) {
      if (anchor.pathname == url) {
        anchor.classList.add('is-inverted');
      } else {
        anchor.classList.remove('is-inverted');
      }
    });
  }

  var Home = function Home () {};

  Home.prototype.oninit = function oninit () {
    u.setActive('/');
  };
  Home.prototype.view = function view$1 () {
    return view
  };

  var view = m('section.section', m('.container', [
    m('h4.title.is-4', 'Youtube Playlist to MPD'),
    m('cite', 'Please install and use a mpd client.'),
    m('hr'),
    m('form#ytsearch[action="https://www.youtube.com/results"][target="youtube"]', [
      m('.field', [
        m('label.label[for="search_query"]', 'Youtube Search'),
        m('.control',
          m('input#search_query.input[autofocus][name="search_query"][placeholder="band / artist"]')
        )
      ]),
      m('.field',
        m('.control',
          m('button.button.is-link[type="submit"]', 'Search')
        )
      )
    ]),
    m('hr'),
    m('form#ytlist[action="/ytlist/"]', {
      onsubmit: function (ev) {
        ev.preventDefault();
        var id = getParam(u.i('ytlist_id').value);
        u.i('ytlist_id').value = id;
        m.route.set(("/videos/" + id));
      }
    }, [
      m('.field', [
        m('label.label[for="ytlist_id"]', 'Youtube List ID'),
        m('.control', m('input#ytlist_id.input[placeholder="yt list id"]'))
      ]),
      m('.field',
        m('.control',
          m('button.button.is-primary.is-success[type="submit"]', 'Show Videos')
        )
      )
    ])
  ]));

  function getParam(url) {
    url = url.trim().replace(/.*\//, '');
    url = url.replace(/.*watch\?/, '');
    var params = url.split('&').map(function (el) { return el.split('='); });
    if (params.length == 1 && params[0].length == 1) {
      var out$1 = params[0][0];
      if (out$1.length && out$1.length < 12) {
        out$1 = 'RD' + out$1;
      }
      return out$1
    }
    var out = '';
    for (var i = 0; i < params.length; i++) {
      if (params[i][0] == 'v' && out == '') {
        out = params[i][1];
      }
      if (params[i][0] == 'l' || params[i][0] == 'list') {
        out = params[i][1];
      }
    }
    if (out.length && out.length < 12) {
      out = 'RD' + out;
    }
    return out
  }

  var VideoView = function VideoView () {};

  VideoView.prototype.view = function view (vnode) {
    var id = vnode.attrs.id;
    var idx = vnode.attrs.idx;
    var href = vnode.attrs.isSong? '/songs/' + idx: 'https://youtu.be/' + id;
    var duration = vnode.attrs.duration;
    var title = vnode.attrs.title + ' (' + duration + ')';
    // const thumb = vnode.attrs.thumbnail
    var thumb = 'https://img.youtube.com/vi/' + id + '/0.jpg';
    var extraClass = vnode.attrs.selected? '.selected': '';

    var lastOne = m('a.related[title=Related][href=/videos/RD' + id + ']', 'show related videos');
    var footer;
    if (vnode.attrs.withoutCheck) {
      footer = lastOne;
    } else {
      footer = [
        m('input[id=check_+' + id + '][type=checkbox][checked][data-id=' + id + '][data-title=' + JSON.stringify(title) + ']'),
        m('label[for=check_+' + id + ']', 'select this video'),
        m('br'),
        lastOne
      ];
    }

    return m('.column.is-one-quarter-desktop.is-half-tablet' + extraClass, m('.card', [
      m('.card-image',
        m('figure.image is-4by3',
          m('a[target=youtube][title=' + title + '][href=' + href + '][data-idx=' + idx + ']',
            m('img[alt=' + title + '][src=' + thumb + ']')
          )
        )
      ),
      m('.card-content',
        m('.media-content',
          m('.title.is-6.has-text-centered',
            m('a[target=youtube][title=' + title + '][href=' + href + '][data-idx=' + idx + ']', title)
          )
        )
      ),
      m('.content',
        m('.title.is-6.has-text-centered', footer)
      )
    ]))
  };

  var state = {
    data: {},
    id: ''
  };

  var Videos = function Videos () {};

  Videos.prototype.oninit = function oninit (vnode) {
    u.setActive('');
    this.update(vnode);
  };
  Videos.prototype.onupdate = function onupdate (vnode) {
    this.update(vnode);
  };
  Videos.prototype.update = function update (vnode) {
    if (state.id == vnode.attrs.id) {
      return
    }
    state.id = vnode.attrs.id;
    m.request(u.base + '/ytlist/' + state.id).then(function (data) {
      state.data = data;
    });
  };
  Videos.prototype.view = function view () {
    var isLoading = '.is-loading';
    var videos = [];
    if (state.data.title) {
      isLoading = '';
      videos = state.data.video;
    }
    return m('section.section', m('.container', [
      m('.has-text-centered', 
        m('button.button.is-primary.is-large' + isLoading, {onclick: sendMusic}, 'Send Music')
      ),
      m('#videos.columns.is-tablet.is-multiline', videos.map(function (video, idx) { return m(VideoView, {
        id: video.encrypted_id,
        duration: video.duration,
        title: video.title,
        thumbnail: video.thumbnail,
        idx: idx
      }); }))
    ]))
  };

  function sendMusic(ev) {
    ev.preventDefault();
    var checks = document.querySelectorAll('#videos input[type=checkbox]:checked');
    var data = [];
    checks.forEach(function (check) {
      data.push({
        id: check.dataset.id,
        title: check.dataset.title
      });
    });
    return m.request(u.base + '/list', {
      method: 'POST',
      // headers: { 'Content-Type': 'application/json'},
      data: data
    })
  }

  var Volume = function Volume () {};

  Volume.prototype.oninit = function oninit (vnode) {
    this.state = { volume: formatVolume(vnode.attrs.status) };
  };

  Volume.prototype.view = function view () {
    return m('.buttons.has-addons.is-centered', [
      m('a.button[href="/voldown"]', { onclick: this.setVolume.bind(this) }, '⇓'),
      m('button#volLabel.button[disabled]', {
        onclick: function (ev) { return ev.preventDefault(); }
      }, 'Volume: ' + this.state.volume),
      m('a.button[href="/volup"]', { onclick: this.setVolume.bind(this) }, '⇑')
    ])
  };

  Volume.prototype.setVolume = function setVolume (ev) {
      var this$1 = this;

    ev.preventDefault();
    var url = ev.target.pathname;
    return m.request(u.base + url).then(function (volume) {
      this$1.state.volume = formatVolume(volume);
    })
  };

  function formatVolume(volume) {
    if (volume == 100) { volume = 99; }
    if (volume < 10) { volume = '0' + volume; }
    return volume
  }

  var state$1 = {
    data: {}
  };

  var Songs = function Songs () {};

  Songs.prototype.oninit = function oninit () {
    u.setActive('/songs');
    state$1.data = {};
    m.request(u.base + '/playlist').then(function (data) {state$1.data = data;});
  };
  Songs.prototype.onupdate = function onupdate () {
    u.setActive('/songs');
  };
  Songs.prototype.view = function view (vnode) {
    if (!state$1.data.songs) {
      return m('.has-text-centered', '…')
    }
    var songs = state$1.data.songs.filter(function (s) { return s; });
    var sel = state$1.data.status.song;
    if (typeof vnode.attrs.idx != 'undefined') {
      sel = vnode.attrs.idx;
      m.request(u.base + '/seek/' + sel);
    }
    return m('section.section', { onclick: this.onclick.bind(this) }, [
      m('.buttons.has-addons.is-centered', [
        m('a.button[href="/prev"]', { onclick: this.moveSong.bind(this) }, '⇐Prev'),
        m('a.button[href="/next"]', { onclick: this.moveSong.bind(this) }, 'Next⇒')
      ]),
      m('.container', [
        m(Volume, { status: state$1.data.status.volume }),
        m('#songs.columns.is-tablet.is-multiline', songs.map(function (s, i) { return songView(sel, i, s); }))
      ])
    ])
  };

  Songs.prototype.moveSong = function moveSong (ev) {
    ev.preventDefault();
    var url = ev.target.pathname;
    return m.request(u.base + url).then(selectByStatus)
  };

  Songs.prototype.onclick = function onclick (ev) {
    var target = ev.target.tagName == 'IMG' ? ev.target.parentNode: ev.target;
    if (target.tagName == 'A' && target.target == 'youtube') {
      ev.preventDefault();
      var url = '/seek/' + target.dataset.idx;
      return m.request(u.base + url).then(selectByStatus)
    }
  };

  function selectByStatus(status) {
    var old = u.q('#songs .column.selected');
    var sel = u.q('#songs .column:nth-child(' + (status.song+1) + ')');
    old.classList.remove('selected');
    sel.classList.add('selected');
  }

  function songView(sel, idx, song) {
    var assign;

    var reg = /_\([0-9:]+\);;;[0-9a-zA-Z_-]+/;
    var songSplit = song.split('&');
    song = songSplit[songSplit.length - 1];
    var match = song.match(reg);

    var duration = '';
    var id = '';

    if (match) {
      var idx$1 = song.lastIndexOf(match[match.length-1]);
      var extra = song.substr(idx$1+1);
      if (extra) {
        (assign = extra.split(';;;'), duration = assign[0], id = assign[1]);
        duration = duration.replace(/(\(|\))?/g, '');
      }
      song = song.substr(0, idx$1);
    }
    song = song.replace(/_/g, ' ');
    // const title = song.replace(reg, '')
    return m(VideoView, {
      id: id, duration: duration,
      title: song,
      thumbnail: '',
      selected: sel==idx,
      withoutCheck: true,
      idx: idx
    })
  }

  var Options = function Options () {};

  Options.prototype.oninit = function oninit () {
      var this$1 = this;

    this.state = {
      options: {
        mpd: {
          host: '',
          port: ''
        }
      }
    };
    u.setActive('/options');
    m.request(u.base + '/options')
      .then(function (opts) {
        this$1.state.options = opts;
      });
  };
  Options.prototype.view = function view () {
    var mpd_host = this.state.options.mpd.host;
    var mpd_port = this.state.options.mpd.port;
    u.l('mpd_host', mpd_host);
    return m('section.section', m('.container.has-text-centered', [
      m('h4.title.is-4', 'Options'),
      m('form#options[method=POST][action=/api/options]', {onsubmit: jsonSubmit}, [
        m('fieldset', [
          m('legend.title.is-5', 'MPD'),
          m('.field', [
            m('label.label[for=mpd_host]', 'Host'),
            m('input.input[type=text][id=mpd_host][name=mpd_host][value=' + mpd_host + '][placeholder=localhost]')
          ]),
          m('.field', [
            m('label.label[for=mpd_port]', 'Port'),
            m('input.input[type=text][id=mpd_port][name=mpd_port][value=' + mpd_port + '][placeholder=6600]')
          ])
        ]),
        m('.field', [
          m('label.label[for=global_base]', 'Base'),
          m('input.input[type=text][id=global_base][name=global_base][value=' + u.base + '][placeholder=/api]')
        ]),
        m('.field',
          m('.control',
            m('button.button.is-link[type="submit"]', 'Submit')
          )
        )
      ])
    ]))
  };

  function jsonSubmit(ev) {
    ev.preventDefault();
    var data = {};
    ev.target.querySelectorAll('input').forEach(function (el) {
      var name = el.name.split('_');
      if (name.length == 0) { return }
      if (name.length > 1 && name[0] == 'global') {
        window.localStorage.setItem('yt2mpd_' + name[1], getValue(el));
        return
      }
      if (name.length == 1) {
        data[name[0]] = getValue(el);
        return
      }
      data[name[0]] = data[name[0]] || {};
      data[name[0]][name[1]] = getValue(el);
    });
    var method = 'POST';
    m.request(u.base + '/options', { method: method, data: data });
  }

  function getValue(el) {
    var value = el.type == 'checkbox'? el.checked: el.value;
    if (!isNaN(value)) { value = parseFloat(value); }
    return value
  }

  var root = u.i('main');

  m.route.prefix('');

  m.route(root, '/', {
    '/': Home,
    '/videos/:id': Videos,
    '/songs/:idx': Songs,
    '/songs': Songs,
    '/options': Options
  });

  // BEGIN navigation menu link types
  u.a('#navMenu a.button').forEach(function (element) {
    element.addEventListener('click', function (ev) {
      ev.preventDefault();
      var url = element.pathname;
      var type = element.dataset.type;
      switch (type) {
      case 'xhr':
        m.request(u.base + url);
        break
      case 'route':
        m.route.set(url);
        break
      }
    });
  });
  // END navigation menu link types
  // BEGIN delegate related videos
  u.i('main').addEventListener('click', function (ev) {
    if (ev.target.tagName == 'A' && ev.target.classList.contains('related')) {
      ev.preventDefault();
      var url = ev.target.href;
      // const id = ev.target.dataset.id
      m.route.set(url);
    }
  });
  // END delegate related videos

}());
