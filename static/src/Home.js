import u from './_utils'

export default class Home {
  oninit() {
    u.setActive('/')
  }
  view() {
    return view
  }
}

const view = m('section.section', m('.container', [
  m('h4.title.is-4', 'Youtube Playlist to MPD'),
  m('cite', 'Please install and use a mpd client.'),
  m('hr'),
  m('form#ytsearch[action="https://www.youtube.com/results"][target="youtube"]', [
    m('.field', [
      m('label.label[for="search_query"]', 'Youtube Search'),
      m('.control',
        m('input#search_query.input[autofocus][name="search_query"][placeholder="band / artist"]')
      )
    ]),
    m('.field',
      m('.control',
        m('button.button.is-link[type="submit"]', 'Search')
      )
    )
  ]),
  m('hr'),
  m('form#ytlist[action="/ytlist/"]', {
    onsubmit: ev => {
      ev.preventDefault()
      const id = getParam(u.i('ytlist_id').value)
      u.i('ytlist_id').value = id
      m.route.set(`/videos/${id}`)
    }
  }, [
    m('.field', [
      m('label.label[for="ytlist_id"]', 'Youtube List ID'),
      m('.control', m('input#ytlist_id.input[placeholder="yt list id"]'))
    ]),
    m('.field',
      m('.control',
        m('button.button.is-primary.is-success[type="submit"]', 'Show Videos')
      )
    )
  ])
]))

function getParam(url) {
  url = url.trim().replace(/.*\//, '')
  url = url.replace(/.*watch\?/, '')
  const params = url.split('&').map(el => el.split('='))
  if (params.length == 1 && params[0].length == 1) {
    let out = params[0][0]
    if (out.length && out.length < 12) {
      out = 'RD' + out
    }
    return out
  }
  let out = ''
  for (let i = 0; i < params.length; i++) {
    if (params[i][0] == 'v' && out == '') {
      out = params[i][1]
    }
    if (params[i][0] == 'l' || params[i][0] == 'list') {
      out = params[i][1]
    }
  }
  if (out.length && out.length < 12) {
    out = 'RD' + out
  }
  return out
}
