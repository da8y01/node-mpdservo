import u from './_utils'

export default class Options {
  oninit() {
    this.state = {
      options: {
        mpd: {
          host: '',
          port: ''
        }
      }
    }
    u.setActive('/options')
    m.request(u.base + '/options')
      .then(opts => {
        this.state.options = opts
      })
  }
  view() {
    const mpd_host = this.state.options.mpd.host
    const mpd_port = this.state.options.mpd.port
    u.l('mpd_host', mpd_host)
    return m('section.section', m('.container.has-text-centered', [
      m('h4.title.is-4', 'Options'),
      m('form#options[method=POST][action=/api/options]', {onsubmit: jsonSubmit}, [
        m('fieldset', [
          m('legend.title.is-5', 'MPD'),
          m('.field', [
            m('label.label[for=mpd_host]', 'Host'),
            m('input.input[type=text][id=mpd_host][name=mpd_host][value=' + mpd_host + '][placeholder=localhost]')
          ]),
          m('.field', [
            m('label.label[for=mpd_port]', 'Port'),
            m('input.input[type=text][id=mpd_port][name=mpd_port][value=' + mpd_port + '][placeholder=6600]')
          ])
        ]),
        m('.field', [
          m('label.label[for=global_base]', 'Base'),
          m('input.input[type=text][id=global_base][name=global_base][value=' + u.base + '][placeholder=/api]')
        ]),
        m('.field',
          m('.control',
            m('button.button.is-link[type="submit"]', 'Submit')
          )
        )
      ])
    ]))
  }
}

function jsonSubmit(ev) {
  ev.preventDefault()
  let data = {}
  ev.target.querySelectorAll('input').forEach(el => {
    const name = el.name.split('_')
    if (name.length == 0) return
    if (name.length > 1 && name[0] == 'global') {
      window.localStorage.setItem('yt2mpd_' + name[1], getValue(el))
      return
    }
    if (name.length == 1) {
      data[name[0]] = getValue(el)
      return
    }
    data[name[0]] = data[name[0]] || {}
    data[name[0]][name[1]] = getValue(el)
  })
  const method = 'POST'
  m.request(u.base + '/options', { method, data })
}

function getValue(el) {
  let value = el.type == 'checkbox'? el.checked: el.value
  if (!isNaN(value)) value = parseFloat(value)
  return value
}
