import Home from './Home'
import Videos from './Videos'
import Songs from './Songs'
import Options from './Options'
import u from './_utils'

const root = u.i('main')

m.route.prefix('')

m.route(root, '/', {
  '/': Home,
  '/videos/:id': Videos,
  '/songs/:idx': Songs,
  '/songs': Songs,
  '/options': Options
})

// BEGIN navigation menu link types
u.a('#navMenu a.button').forEach(element => {
  element.addEventListener('click', ev => {
    ev.preventDefault()
    const url = element.pathname
    const type = element.dataset.type
    switch (type) {
    case 'xhr':
      m.request(u.base + url)
      break
    case 'route':
      m.route.set(url)
      break
    }
  })
})
// END navigation menu link types
// BEGIN delegate related videos
u.i('main').addEventListener('click', ev => {
  if (ev.target.tagName == 'A' && ev.target.classList.contains('related')) {
    ev.preventDefault()
    const url = ev.target.href
    // const id = ev.target.dataset.id
    m.route.set(url)
  }
})
// END delegate related videos
