import VideoView from './components/VideoView'
import Volume from './components/Volume'
import u from './_utils'

let state = {
  data: {}
}

export default class Songs {
  oninit() {
    u.setActive('/songs')
    state.data = {}
    m.request(u.base + '/playlist').then(data => {state.data = data})
  }
  onupdate() {
    u.setActive('/songs')
  }
  view(vnode) {
    if (!state.data.songs) {
      return m('.has-text-centered', '…')
    }
    const songs = state.data.songs.filter(s => s)
    let sel = state.data.status.song
    if (typeof vnode.attrs.idx != 'undefined') {
      sel = vnode.attrs.idx
      m.request(u.base + '/seek/' + sel)
    }
    return m('section.section', { onclick: this.onclick.bind(this) }, [
      m('.buttons.has-addons.is-centered', [
        m('a.button[href="/prev"]', { onclick: this.moveSong.bind(this) }, '⇐Prev'),
        m('a.button[href="/next"]', { onclick: this.moveSong.bind(this) }, 'Next⇒')
      ]),
      m('.container', [
        m(Volume, { status: state.data.status.volume }),
        m('#songs.columns.is-tablet.is-multiline', songs.map((s, i) => songView(sel, i, s)))
      ])
    ])
  }

  moveSong(ev) {
    ev.preventDefault()
    const url = ev.target.pathname
    return m.request(u.base + url).then(selectByStatus)
  }

  onclick(ev) {
    const target = ev.target.tagName == 'IMG' ? ev.target.parentNode: ev.target
    if (target.tagName == 'A' && target.target == 'youtube') {
      ev.preventDefault()
      const url = '/seek/' + target.dataset.idx
      return m.request(u.base + url).then(selectByStatus)
    }
  }
}

function selectByStatus(status) {
  const old = u.q('#songs .column.selected')
  const sel = u.q('#songs .column:nth-child(' + (status.song+1) + ')')
  old.classList.remove('selected')
  sel.classList.add('selected')
}

function songView(sel, idx, song) {
  const reg = /_\([0-9:]+\);;;[0-9a-zA-Z_-]+/
  const songSplit = song.split('&')
  song = songSplit[songSplit.length - 1]
  const match = song.match(reg)

  let duration = ''
  let id = ''

  if (match) {
    const idx = song.lastIndexOf(match[match.length-1])
    const extra = song.substr(idx+1)
    if (extra) {
      [duration, id] = extra.split(';;;')
      duration = duration.replace(/(\(|\))?/g, '')
    }
    song = song.substr(0, idx)
  }
  song = song.replace(/_/g, ' ')
  // const title = song.replace(reg, '')
  return m(VideoView, {
    id, duration,
    title: song,
    thumbnail: '',
    selected: sel==idx,
    withoutCheck: true,
    idx
  })
}
