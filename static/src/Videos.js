import VideoView from './components/VideoView'
import u from './_utils'

let state = {
  data: {},
  id: ''
}

export default class Videos { 
  oninit(vnode) {
    u.setActive('')
    this.update(vnode)
  }
  onupdate(vnode) {
    this.update(vnode)
  }
  update(vnode) {
    if (state.id == vnode.attrs.id) {
      return
    }
    state.id = vnode.attrs.id
    m.request(u.base + '/ytlist/' + state.id).then(data => {
      state.data = data
    })
  }
  view() {
    let isLoading = '.is-loading'
    let videos = []
    if (state.data.title) {
      isLoading = ''
      videos = state.data.video
    }
    return m('section.section', m('.container', [
      m('.has-text-centered', 
        m('button.button.is-primary.is-large' + isLoading, {onclick: sendMusic}, 'Send Music')
      ),
      m('#videos.columns.is-tablet.is-multiline', videos.map((video, idx) => m(VideoView, {
        id: video.encrypted_id,
        duration: video.duration,
        title: video.title,
        thumbnail: video.thumbnail,
        idx
      })))
    ]))
  }
}

function sendMusic(ev) {
  ev.preventDefault()
  const checks = document.querySelectorAll('#videos input[type=checkbox]:checked')
  let data = []
  checks.forEach(check => {
    data.push({
      id: check.dataset.id,
      title: check.dataset.title
    })
  })
  return m.request(u.base + '/list', {
    method: 'POST',
    // headers: { 'Content-Type': 'application/json'},
    data
  })
}
