export default class VideoView {
  view(vnode) {
    const id = vnode.attrs.id
    const idx = vnode.attrs.idx
    const href = vnode.attrs.isSong? '/songs/' + idx: 'https://youtu.be/' + id
    const duration = vnode.attrs.duration
    const title = vnode.attrs.title + ' (' + duration + ')'
    // const thumb = vnode.attrs.thumbnail
    const thumb = 'https://img.youtube.com/vi/' + id + '/0.jpg'
    const extraClass = vnode.attrs.selected? '.selected': ''

    let lastOne = m('a.related[title=Related][href=/videos/RD' + id + ']', 'show related videos')
    let footer
    if (vnode.attrs.withoutCheck) {
      footer = lastOne
    } else {
      footer = [
        m('input[id=check_+' + id + '][type=checkbox][checked][data-id=' + id + '][data-title=' + JSON.stringify(title) + ']'),
        m('label[for=check_+' + id + ']', 'select this video'),
        m('br'),
        lastOne
      ]
    }

    return m('.column.is-one-quarter-desktop.is-half-tablet' + extraClass, m('.card', [
      m('.card-image',
        m('figure.image is-4by3',
          m('a[target=youtube][title=' + title + '][href=' + href + '][data-idx=' + idx + ']',
            m('img[alt=' + title + '][src=' + thumb + ']')
          )
        )
      ),
      m('.card-content',
        m('.media-content',
          m('.title.is-6.has-text-centered',
            m('a[target=youtube][title=' + title + '][href=' + href + '][data-idx=' + idx + ']', title)
          )
        )
      ),
      m('.content',
        m('.title.is-6.has-text-centered', footer)
      )
    ]))
  }
}
