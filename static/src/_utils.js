const i = document.getElementById.bind(document)
const q = document.querySelector.bind(document)
const a = document.querySelectorAll.bind(document)
const c = document.createElement.bind(document)
const t = document.createTextNode.bind(document)
// eslint-disable-next-line no-console
const l = console.log.bind(console)
// eslint-disable-next-line no-console
const e = console.error.bind(console)

let base = window.localStorage.getItem('yt2mpd_base') || '/api'
window.localStorage.setItem('yt2mpd_base', base)

export default {
  i, q, a, c, t, l, e,
  setActive,
  base
}

function setActive(url) {
  a('#navMenu a[data-type=route]').forEach(anchor => {
    if (anchor.pathname == url) {
      anchor.classList.add('is-inverted')
    } else {
      anchor.classList.remove('is-inverted')
    }
  })
}
