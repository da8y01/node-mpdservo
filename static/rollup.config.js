import { uglify } from 'rollup-plugin-uglify'
import buble from 'rollup-plugin-buble'

const base = {
  input: __dirname + '/src/index.js',
  external: ['mithril'],
  plugins: [
    buble(),
  ],
  output: {
    file: __dirname + '/main.js',
    format: 'iife',
    globals: {
      mithril: 'm'
    }
  }
}

const uglified = {
  input: __dirname + '/main.js',
  plugins: [
    uglify()
  ],
  output: {
    file: __dirname + '/main.min.js',
    format: 'iife'
  }
}

export default [
  base
  //, uglified
]
